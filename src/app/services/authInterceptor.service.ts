import { Injectable, Injector } from '@angular/core'
import { HttpInterceptor} from '@angular/common/http'
import { read } from 'fs';
import { AuthService } from './auth.service';


@Injectable()
export class AuthInterceptorService implements HttpInterceptor {

    constructor(private _injector: Injector){}

    intercept(req, next){
        var authService = this._injector.get(AuthService)
        var authReq = req.clone({
            headers: req.headers.set('Authorization', 'token ' + authService.token)
        })

        return next.handle(authReq)
    }
}