import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable()
export class AuthService{

    private apiUrl = 'http://localhost:3000/auth'
    TOKEN_KEY = 'token'
    constructor(private _http: HttpClient){
    }    

    get token(){
        return localStorage.getItem(this.TOKEN_KEY)
    }

    registerUser(newUser){
        this._http.post(this.apiUrl + '/register', newUser)
                .subscribe(res => {                    
                })
    }

    loginUser(loginData){

        this._http.post<any>(this.apiUrl + '/login', loginData)
            .subscribe(res => {
                localStorage.setItem(this.TOKEN_KEY, res.token)
            })
    }
}