import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable()
export class ApiService{

    private apiUrl = 'http://localhost:3000'
    messages = [] 
    users = [] 

    constructor(private _http: HttpClient){
    }

    getMessages(userId){
        this._http.get<any>(this.apiUrl +  '/posts/' + userId)
                .subscribe(res => {
                    this.messages = res
                })
    }

    postMessage(message){
        return this._http.post(this.apiUrl +  '/post', message)
    }

    getUsers(){
        this._http.get<any>(this.apiUrl +  '/users')
                .subscribe(res => {
                    this.users = res
                })
    }

    getProfile(id){
        return this._http.get(this.apiUrl +  '/profile/' + id)                
    }
}