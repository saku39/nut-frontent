import { BrowserModule } from '@angular/platform-browser'
import { NgModule } from '@angular/core'

import { AppComponent } from './app.component';
import { ApiService } from './services/api.service'
import { AuthService} from './services/auth.service'
import { AuthInterceptorService} from './services/authInterceptor.service'
import { BrowserAnimationsModule} from '@angular/platform-browser/animations'
import { RouterModule} from '@angular/router'
import {
    MatButtonModule, 
    MatCheckboxModule, 
    MatCardModule, 
    MatToolbarModule,
    MatInputModule,
    MatListModule} from '@angular/material';
import { 
    MessagesComponent, 
    RegisterComponent, 
    NavComponent, 
    LoginComponent, 
    UsersComponent,
    ProfileComponent,
    PostComponent
  } from './components';
import { FormsModule } from '@angular/forms'
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http'

const routes = [  
  { path: '', component: PostComponent},
  { path:'register', component: RegisterComponent},
  { path: 'login', component: LoginComponent},
  { path: 'users', component: UsersComponent},
  { path: 'profile/:id', component: ProfileComponent}
]

@NgModule({
  declarations: [
    AppComponent,
    MessagesComponent,
    RegisterComponent,
    NavComponent,
    LoginComponent,
    UsersComponent,
    ProfileComponent,
    PostComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    BrowserAnimationsModule, 
    RouterModule.forRoot(routes),
    MatButtonModule, 
    MatCheckboxModule, 
    MatCardModule, 
    MatToolbarModule,
    MatInputModule, 
    FormsModule,
    MatListModule
  ],
  providers: [ 
    ApiService, 
    AuthService, 
    { 
      provide: HTTP_INTERCEPTORS, 
      useClass: AuthInterceptorService,
      multi: true
    }],
  bootstrap: [AppComponent]
})
export class AppModule { }
