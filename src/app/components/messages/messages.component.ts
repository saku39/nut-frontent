import { Component } from '@angular/core'
import { ApiService } from '../../services/api.service'
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'nut-messages',
  template:`
  <mat-card *ngFor="let post of _apiService.messages">
      {{post.msg}}
    </mat-card>
  ` 
})
export class MessagesComponent {
  
    constructor(private _apiService: ApiService, private route: ActivatedRoute){       
    }

    ngOnInit(){
      let userId = this.route.snapshot.paramMap.get('id')
      this._apiService.getMessages(userId)
    }
}
