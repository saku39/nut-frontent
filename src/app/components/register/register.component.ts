import { Component } from '@angular/core';
import { AuthService } from '../../services/auth.service';

@Component({
  selector: 'nut-register',
  templateUrl: './register.component.html'
})
export class RegisterComponent {

  registerData ={};
  constructor(private _authService: AuthService){
    
  }

  register(){
      this._authService.registerUser(this.registerData)
  }
}