import { Component } from '@angular/core'
import { ApiService } from '../../services/api.service'

@Component({
  selector: 'nut-post',
  templateUrl: './post.component.html' 
})
export class PostComponent {
  
    post = {}
    constructor(private _apiService: ApiService){       
    }

    postMessage(){
      this._apiService.postMessage(this.post)
            .subscribe(res => {
                console.log('message posted')
                this.post = {}
            })      
    }
}
