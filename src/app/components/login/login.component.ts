import { Component } from '@angular/core';
import { AuthService } from '../../services/auth.service';

@Component({
  selector: 'nut-login',
  templateUrl: './login.component.html'
})
export class LoginComponent {
    loginData ={};

    constructor(private _authService: AuthService){       
    }


    login(){
        this._authService.loginUser(this.loginData);
        this.loginData = {}
        alert('Success')
    }
}