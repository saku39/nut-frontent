import { Component } from '@angular/core';

@Component({
  selector: 'nut-nav',
  template: `
    <mat-toolbar>
      <button mat-button routerLink="/">Toolbar</button>
      <span style="flex: 1 1 auto"></span>
      <button mat-button routerLink="/users">Users</button>
      <button mat-button routerLink="/login">Login</button>
      <button mat-button routerLink="/register">Registrar</button>
    </mat-toolbar>
  `
})
export class NavComponent {
   
}