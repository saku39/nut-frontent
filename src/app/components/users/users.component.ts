import { Component } from '@angular/core';
import { ApiService } from '../../services/api.service';


@Component({
  selector: 'nut-users',
  template: `
    <div *ngFor="let user of _apiService.users">
        <mat-card 
            [routerLink]="['/profile', user._id]"
            style="cursor:pointer">
                {{user.name}} - {{user.email}}
        </mat-card>
        <br>
    </div>
  `
})
export class UsersComponent {
       
    constructor(private _apiService: ApiService){

    }
    
    ngOnInit(): void {
        this._apiService.getUsers()
    }
}