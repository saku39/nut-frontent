import { Component } from '@angular/core'
import{ ActivatedRoute} from '@angular/router'

import { ApiService } from '../../services/api.service'


@Component({
  selector: 'nut-profile',
  templateUrl: './profile.component.html'
})
export class ProfileComponent {

    profile = {}

    constructor(
        private _apiServce: ApiService, 
        private _route: ActivatedRoute){       
    }

    ngOnInit(){

        var id = this._route.snapshot.params.id

        this._apiServce.getProfile(id)
            .subscribe(data => this.profile = data)
    }    
}